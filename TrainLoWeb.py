#!/usr/bin/python

import json
import yaml
import os

from flask import Flask
app = Flask(__name__)

stream = open('config.yml', 'r')
config = yaml.load(stream)
stream.close()

@app.route("/train_tracks/bubbles/<username>/<password>")
def gps_bubbles(username, password):
    for user in config['users']:
        if user['username'] == username and user['password'] == password:
           return json.dumps(config['bubbles'])
        else:
           return '{error:1}'

@app.route("/train_tracks/bubble/<username>/<password>/<int:bubble_id>")
def show_location(username, password, bubble_id):
    for user in config['users']:
        if user['username'] == username and user['password'] == password:
           for bubble in config['bubbles']:
               if bubble['id'] == bubble_id:
                   os.system("/usr/bin/notify-send 'Streetcar Notification' 'Streetcar "  + username + " is near " + bubble['name'] + "'")
    return '{success:1}'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=config['port'], debug=config['debug'])
